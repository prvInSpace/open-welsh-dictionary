package cymru.prv.dictionary.welsh.tenses;

import cymru.prv.dictionary.welsh.WelshVerb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


/**
 * Represents the future tense in Welsh
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class FutureWelshVerbTense extends WelshVerbTense {

    public FutureWelshVerbTense(WelshVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Arrays.asList(apply("af"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Arrays.asList(apply("i"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Arrays.asList(
                apply("iff"),
                apply("ith")
        );
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Arrays.asList(apply("wn"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Arrays.asList(apply("wch"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Arrays.asList(apply("an"));
    }
}
