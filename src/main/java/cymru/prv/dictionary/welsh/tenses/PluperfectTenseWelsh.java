package cymru.prv.dictionary.welsh.tenses;

import cymru.prv.dictionary.welsh.WelshVerb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;


/**
 * Represents the pluperfect tense in Welsh
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class PluperfectTenseWelsh extends WelshVerbTense {

    public PluperfectTenseWelsh(WelshVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Arrays.asList(apply("aswn"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Arrays.asList(apply("asit"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Arrays.asList(apply("asai"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Arrays.asList(apply("asem"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Arrays.asList(apply("asech"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Arrays.asList(apply("asent"));
    }
}
