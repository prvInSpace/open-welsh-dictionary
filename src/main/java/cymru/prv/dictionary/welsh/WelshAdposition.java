package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.Inflection;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.List;


/**
 * Represents a Welsh adposition
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class WelshAdposition extends Word {

    private Inflection inflection;

    public WelshAdposition(JSONObject obj) {
        super(obj, WordType.adposition);
        if(obj.optBoolean("inflected", false))
            inflection = new Inflection(obj);
    }

    @Override
    protected JSONObject getInflections() {
        if(inflection != null)
            return inflection.toJson();
        return null;
    }

    @Override
    public List<String> getVersions() {
        var list = super.getVersions();
        if(inflection != null)
            list.addAll(inflection.getVersions());
        return list;
    }
}
